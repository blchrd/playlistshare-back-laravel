<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('iconcaptcha_challenges', function (Blueprint $table) {
            $table->id();
            $table->string("challenge_id");
            $table->string("widget_id");
            $table->string("puzzle");
            $table->string("ip_address");
            $table->integer("expires_at");
            $table->timestamps();
            $table->unique(['challenge_id', 'widget_id'], 'iconcaptcha_challenges_widget');
        });

        Schema::create('iconcaptcha_attempts', function (Blueprint $table) {
            $table->id();
            $table->string("ip_address");
            $table->integer("attempts");
            $table->integer("timeout_until");
            $table->integer("valid_until");
            $table->unique(['ip_address'], 'iconcaptcha_attempts_ip_address');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('iconcaptcha_challenges');
        Schema::dropIfExists('iconcaptcha_attempts');
    }
};
