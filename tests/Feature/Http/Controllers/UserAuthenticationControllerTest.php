<?php

namespace Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserAuthenticationControllerTest extends TestCase
{
    // public function testUserIsCreatedSuccessfully()
    public function userIsCreatedSuccessfully()
    {
        //TODO: Make this test works with IconCaptcha
        $this->withoutMiddleware();
        $payload = [
            'name' => fake()->name,
            'email' => fake()->email,
            'password' => '12345678',
            'password_confirmation' => '12345678',
        ];
        $this->json('post', 'api/v1/register', $payload)
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(
                [
                    'message',
                    'access_token',
                    'token_type',
                ]
            );

        $payloadWithoutPassword = [
            'name' => $payload["name"],
            'email' => $payload["email"],
        ];
        $this->assertDatabaseHas('users', $payloadWithoutPassword);
    }

    public function testAuthenticationSuccess()
    {
        $name = fake()->lastName;
        $email = fake()->email;
        $password = '12345678';

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);
        $user->markEmailAsVerified();

        $payload = [
            "email" => $email,
            "password" => '12345678',
            "device_name" => 'test',
        ];

        $this->json('post', 'api/v1/login', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                [
                    'access_token',
                    'token_type',
                ]
            );
    }

    public function testAuthenticationUnverifiedMail() {
        $name = fake()->lastName;
        $email = fake()->email;
        $password = '12345678';

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $payload = [
            "email" => $email,
            "password" => '12345678',
            "device_name" => 'test',
        ];

        $this->json('post', 'api/v1/login', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    'message' => 'Account is not verified yet',
                ]
            );
    }

    public function testAuthenticationInvalidCredential()
    {
        $payload = [
            "email" => 'none',
            "password" => '1',
            "device_name" => 'test',
        ];

        $this->json('post', 'api/v1/login', $payload)
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(
                [
                    'message' => 'Invalid login credentials'
                ]
            );
    }

    public function testLogout()
    {
        $this->withMiddleware();
        $name = fake()->lastName;
        $email = fake()->email;
        $password = '12345678';

        User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make($password),
        ]);

        $payload = [
            "email" => $email,
            "password" => '12345678',
            "device_name" => 'test',
        ];
        $response = $this->json('post', 'api/v1/login', $payload)
            ->assertStatus(200);
        $token = $response->json('token');

        $this->json('post', 'api/v1/logout', ['Authorization' => 'Bearer ' . $token])
            ->assertStatus(200)
            ->assertJson([
                'message' => 'Succesfully Logged out'
            ]);
    }
}
