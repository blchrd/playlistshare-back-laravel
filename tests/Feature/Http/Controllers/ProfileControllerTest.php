<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProfileControllerTest extends TestCase {
    public function testUserProfileReturnDataInValidFormat() {
        $user = User::all()->first();
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $json = $this->json('get', "api/v1/user/profile");
        $json->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                ['data' => [
                    'id',
                    'name',
                    'email',
                    'is_admin',
                    ],
                ]
            );
    }

    public function testUpdateUserProfile() {
        $user = User::all()->first();
        $user->markEmailAsVerified();
        $this->actingAs($user, 'sanctum');

        $payload = ['name' => 'joel'];

        $json = $this->json('patch', "api/v1/user/profile", $payload);
        $json->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(
                ['data' => [
                    'id',
                    'name',
                    'email',
                    ],
                ]
            )->assertJson(
                ['data' => ['name' => 'joel']]
            );

        $this->assertDatabaseHas('users', ['id'=>$user->id, 'email'=>$user->email, 'name'=>'joel']);
    }
}
