<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\PlaylistItems;

class FeedController extends Controller
{
    public function feed(Request $request) {
        $appUrl = env("FRONTEND_URL", "");
        $appName = env("APP_NAME");
        $userId = $request->get("user_id");

        // Get all items (filtered by user if necessary)
        $query = PlaylistItems::query()->select('playlist_items.*');
        $query = $query->where("listened", "=", 1);
        $query = $query->where("isPrivate", "=", false);
        if ($userId != null) {
            $query = $query->where("user_id", "=", $userId);
        }
        $query = $query->orderByDesc('listeningDate');

        $items = $query->paginate(10);

        $feedContent = "
        <rss version='2.0'>
        <channel>
            <title>{$appName}</title>
            <link>{$appUrl}</link>
            <description></description>
            <language>en-us</language>";

        // Loop over the content and add it to the feed
        foreach ($items as $item) {
            $fav = "";
            if ($item->favourite) {
                $fav = ", one of the best";
            }
            $genres = [];
            foreach ($item->album->genres as $genre) {
                $genres[] = $genre->genre;
            }
            $genres = htmlentities(join(", ",$genres));
            $comment = "";
            if (!is_null($item->comment)) {
                $comment = app(\Spatie\LaravelMarkdown\MarkdownRenderer::class)->toHtml($item->comment);
            }
            $artist = htmlentities($item->album->artist, ENT_XML1);
            $title = htmlentities($item->album->title, ENT_XML1);
            $authorName = htmlentities($item->user->name, ENT_XML1);

            $feedContent .= "
            <item>
                <title>{$artist} - {$title} ({$genres})</title>
                <description><![CDATA[
                    {$comment}
                    <p>{$item->note} / 5{$fav}<br/>
                    <a href='{$item->album->url}'>Album link</a></p>
                ]]></description>
                <link>{$appUrl}/playlist-item/{$item->id}</link>
                <author>{$authorName}</author>
                <pubDate>{$item->listeningDate}</pubDate>
            </item>";
        }

        $feedContent .= "
        </channel>
        </rss>";

        return response($feedContent, 200)->header('Content-Type', 'application/rss+xml');
    }
}
