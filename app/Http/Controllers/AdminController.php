<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller {
    public function getUserList(Request $request): JsonResponse {
        if (!$request->user()->is_admin) {
            return response()->json([
                'message' => 'Resource not found'
            ], 404);
        }

        $users = User::where('id', '!=', $request->user()->id)->get();
        return response()->json(['data' => $users], 200);
    }

    public function updateUser(Request $request, $userId): JsonResponse {
        if (!$request->user()->is_admin) {
            return response()->json([
                'message' => 'Resource not found'
            ], 404);
        }

        $user = User::find($userId);
        $user->name = $request->input('name', $user->name);
        $user->email = $request->input('email', $user->email);
        $user->save();

        $verified = $request->input('verified');
        if ($verified !== null) {
            if ($verified && $user->email_verified_at == null) {
                $user->markEmailAsVerified();
            } else if (!$verified) {
                $user->email_verified_at = null;
                $user->save();
            }
        }

        return response()->json(['data' => $user], 200);
    }

    public function deleteUser(Request $request, $userId): JsonResponse {
        if (!$request->user()->is_admin) {
            return response()->json([
                'message' => 'Resource not found'
            ], 404);
        }

        // Check playlist for the user
        DB::table('playlist_items')
            ->where('user_id', $userId)
            ->update(array('user_id' => null));

        $user = User::find($userId);
        if ($user !== null) {
            $user->delete();
        } else {
            return response()->json(['message' => 'Error during deletion: User not found'], 404);
        }

        return response()->json(['message' => 'User successfully deleted'], 204);
    }

    public function isUserAdmin(Request $request): JsonResponse {
        return response()->json(['is_admin'=>$request->user()->is_admin]);
    }
}
