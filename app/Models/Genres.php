<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Albums;

class Genres extends Model
{
    use HasFactory;

    protected $fillable = [
        'genre'
    ];

    public function albums()
    {
        return $this->belongsToMany(Albums::class, 'albums_genres', 'genres_id', 'albums_id');
    }
}
