<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PlaylistItems extends Model
{
    use HasFactory;

    protected $fillable = [
        'note',
        'listened',
        'listeningDate',
        'comment',
        'isPrivate',
        'favourite',
    ];

    public function album(): BelongsTo
    {
        return $this->belongsTo(Albums::class, 'albums_id', 'id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
